def ask_for_number_sequence(message):
    myList=[]
    print(message)
    ip_address=input()

    for i in range(len(ip_address.split("."))):
        temp=ip_address.split(".")
        myList.append(int(temp[i]))
    
    return myList
    

def is_valid_ip_address(numberlist):
    isok=True
    if len(numberlist) != 4 :
        isok = False
    else:
        for i in range(len(numberlist)):
          if (-1 < numberlist[i] < 256) == False :
              isok = False
    
    return isok 

def is_valid_netmask(numberlist):
    valid = True
    checking_ones=True
    if len(numberlist) != 4:
        valid = False
    else:
        binary_netmask= ""
        for i in range(len(numberlist)):
            binary = format(numberlist[i],'08b')
            binary_netmask+=binary
        
    for i in range(len(binary_netmask)):
        if int(binary_netmask[i]) == 0:
            checking_ones = False
        if checking_ones == False :
            if int(binary_netmask[i]) == 1:
                valid=False
    return valid
    
def one_bits_in_netmask(netmask):
    count = 0
    netmask_in_binary=""
    for i in range(len(netmask)):
        netmask_in_binary+=format(netmask[i],'08b')
        
    for x in range(len(netmask_in_binary)):
        if int(netmask_in_binary[x]) == 1 :
            count+=1
    return count

def apply_network_mask(host_address,netmask):
    subnet_address = []
    
    for i in range(len(host_address)):
        subnet_element=""
        host_address_binary=""
        netmask_binary=""
        host_address_binary+=format(host_address[i],'08b')
        netmask_binary+=format(netmask[i],'08b')

        for j in range(len(host_address_binary)):
            if (int(host_address_binary[j]) == 1 & int(netmask_binary[j]) == 1):
                subnet_element+="1"
            else:
                subnet_element+="0"
        subnet_address.append(int(subnet_element,2))
    return subnet_address


def netmask_to_wildcard_mask(netmask):
    wildcard_list= []
    for j in range(len(netmask)):
        wildcard_element=""
        for i in f"{netmask[j]:08b}":
            if i == "0" :
                wildcard_element+="1"
            else:
                wildcard_element+="0"
        wildcard_list.append(int(wildcard_element,2))
    
    return wildcard_list

def get_broadcast_address(network_address,wildcard_mask):
    broadcast_address = []
    
    for i in range(len(network_address)):
        broadcast_element=""
        network_address_binary=""
        wildcard_mask_binary=""
        network_address_binary+=format(network_address[i],'08b')
        wildcard_mask_binary+=format(wildcard_mask[i],'08b')

        for j in range(len(network_address_binary)):
            if ((int(network_address_binary[j]) == 1) | (int(wildcard_mask_binary[j]) == 1)):
                broadcast_element+="1"
            else:
                broadcast_element+="0"
        broadcast_address.append(int(broadcast_element,2))
    return broadcast_address



def prefix_length_to_max_hosts(prefix):
    return pow(2,(32-prefix))-2

if __name__ == "__main__":
    ip=ask_for_number_sequence("Wat is het IP-Adres")
    subnetmasker=ask_for_number_sequence("Wat is het Subnetmasker?")
    
    if((is_valid_ip_address(ip)) == True & (is_valid_netmask(subnetmasker)) == True):
        netmasker=apply_network_mask(ip,subnetmasker)
        aantal_hosts=one_bits_in_netmask(subnetmasker)
        wildcardmasker=netmask_to_wildcard_mask(subnetmasker)
        broadcastaddress=get_broadcast_address(netmasker,wildcardmasker)
        aantal_hosts_op_netwerk=prefix_length_to_max_hosts(aantal_hosts)
        print("IP-adres en subnetmasker zijn geldig.")
        print("De lengte van het subnetmasker is " + str(aantal_hosts))
        print("Het adres van het subnet is " + str(netmasker[0:4]))
        print("Het wildcardmasker is " + str(wildcardmasker[0:4]))
        print("Het broadcastadres is " + str(broadcastaddress[0:4]))
        print("Het maximaal aantal hosts op dit subnet is " + str(aantal_hosts_op_netwerk))
    else:
        print("Ip-adres en/of subnetmasker is ongeldig.")
        SystemExit()

#def apply_network_mask(host_address,netmask):
#    host_address_binary=""
#    netmask_binary=""
#    network_mask_binary=""
#    myList=[]
#    
#    for i in range(len(host_address)):
#        host_address_binary+=format(host_address[i],'08b')
#        netmask_binary+=format(netmask[i],'08b')
#    
#    network_mask=f"{int(host_address_binary,2) & int(netmask_binary,2):b}"
#    myList.append(int(network_mask[0:8],2))
#    myList.append(int(network_mask[8:16],2))
#    myList.append(int(network_mask[16:24],2))
#    myList.append(int(network_mask[24:32],2))
#    return myList


#network_address_binary=""
#wildcard_mask_binary=""
#myList=[]
#for i in range(len(network_address)):
#    network_address_binary+=format(network_address[i],'08b')
#    wildcard_mask_binary+=format(wildcard_mask[i],'08b')
#broadcast_address=f"{int(network_address_binary,2) | int(wildcard_mask_binary,2):b}"
#myList.append(int(broadcast_address[0:8],2))
#myList.append(int(broadcast_address[8:16],2))
#myList.append(int(broadcast_address[16:24],2))
#myList.append(int(broadcast_address[24:32],2))
#return myList